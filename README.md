# Introduction  
This is a simple gradle based project for keeping the score in a tennis game.  
It is inspired by https://technologyconversations.com/2014/04/23/java-tutorial-through-katas-tennis-game-easy/

# Tennis scoring rules
The game starts with zero points for each player. Zero is indicated by __LOVE__  
Scoring for a player starting from __LOVE__ proceeds as follows:
* __LOVE__ 
* __FIFTEEN__
* __THIRTY__
* __FOURTY__

When at least 3 points have been scored by each player and the scores are equal,
the score becomes __DEUCE__.  
When a player's current score is __DEUCE__ and that player scores, that player's 
score will become __ADVANTAGE__.  
The game ends when one player has scored at least 4 points and has 2 more points 
than the oponent. This is indicated by the score __END_WIN__

# Usage
Use the gradle wrapper (gradlew/gradlew.bat) to assemble and run the project.

To test:  
```$./gradlew test``` OR  
```$mvn test```

To build:  
```$./gradlew clean build```  
The output of the task is contained in build/ directory  
```$mvn clean install -DskipTests=true```  

To run:  
```$cd build/libs/ && java -jar tennis-kata.jar <args>```  OR  
```$mvn exec:java -Dexec.mainClass="com.entelect.tennis.Main" -Dexec.args="<args>" -DskipTests=true```  

where ```args``` indicate the names of each player (only 2 supported).  
When more than 2 arguments supplied, the first 2 are consumed as the player names  
and the game mode is changed to auto.

## Game modes
The Game has 2 modes of operation:  
* Manual, the user decides which player wins each round.
* Auto, the winner for each round is decided randomly.