package com.entelect.tennis;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.junit.Assert.assertSame;
import static org.mockito.Mockito.*;

public class GameTest {

    private Game game;
    @Mock
    private Player mockPlayer1;
    @Mock
    private Player mockPlayer2;
    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        when(mockPlayer1.getScore()).thenReturn(Score.LOVE);
        when(mockPlayer2.getScore()).thenReturn(Score.LOVE);
        game = new Game(mockPlayer1, mockPlayer2);
    }

    @Test
    public void onNewGame_getScore_returnsLoveLove() {
        Game.GameScore score = game.getScore();

        assertSame(Score.LOVE, score.getP1Score());
        assertSame(Score.LOVE, score.getP2Score());
    }

    @Test
    public void player1WinBall_getScore_returnsFifteen() {
        game.winBall(mockPlayer1);

        verify(mockPlayer1).setScore(Score.FIFTEEN);
    }

    @Test
    public void winBall_currentScoreIsLove_scoreBecomesFifteen() {
        game.winBall(mockPlayer2);

        verify(mockPlayer2).setScore(Score.FIFTEEN);
    }

    @Test
    public void winBall_currentScoreIsFifteen_scoreBecomesThirty() {
        when(mockPlayer1.getScore()).thenReturn(Score.FIFTEEN);

        game.winBall(mockPlayer1);

        verify(mockPlayer1).setScore(Score.THIRTY);
    }

    @Test
    public void winBall_currentScoreIsThirty_scoreBecomesFourty() {
        when(mockPlayer1.getScore()).thenReturn(Score.THIRTY);

        game.winBall(mockPlayer1);

        verify(mockPlayer1).setScore(Score.FOURTY);
    }

    @Test
    public void winBall_scoringPlayerHadThirtyWhileOtherPlayerAlreadyHasFourty_returnDeuceForBoth() {
        when(mockPlayer1.getScore()).thenReturn(Score.THIRTY);
        when(mockPlayer2.getScore()).thenReturn(Score.FOURTY);

        game.winBall(mockPlayer1);

        verify(mockPlayer1).setScore(Score.DEUCE);
        verify(mockPlayer2).setScore(Score.DEUCE);
    }

    @Test
    public void winBall_currentScoreIsDeuce_returnAdvantagePlayer1() {
        when(mockPlayer1.getScore()).thenReturn(Score.DEUCE);

        game.winBall(mockPlayer1);

        verify(mockPlayer1).setScore(Score.ADVANTAGE);
    }

    @Test
    public void winBall_fromAdvantageOtherPlayerScores_bothPlayersScoreReturnToDeuce() {
        when(mockPlayer1.getScore()).thenReturn(Score.ADVANTAGE);
        when(mockPlayer2.getScore()).thenReturn(Score.DEUCE);

        game.winBall(mockPlayer2);

        verify(mockPlayer1).setScore(Score.DEUCE);
        verify(mockPlayer2).setScore(Score.DEUCE);
    }

    @Test
    public void winBall_fromAdvantageScoringEndsGame() {
        when(mockPlayer1.getScore()).thenReturn(Score.ADVANTAGE);
        when(mockPlayer2.getScore()).thenReturn(Score.DEUCE);

        game.winBall(mockPlayer1);

        verify(mockPlayer1).setScore(Score.END_WIN);
    }

    @Test
    public void winBall_fromFourtyWith_endsGame() {
        when(mockPlayer1.getScore()).thenReturn(Score.FOURTY);
        when(mockPlayer2.getScore()).thenReturn(Score.LOVE);

        game.winBall(mockPlayer1);

        verify(mockPlayer1).setScore(Score.END_WIN);
    }
}
