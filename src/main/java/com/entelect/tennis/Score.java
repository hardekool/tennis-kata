package com.entelect.tennis;

public enum Score implements ScoreAdvance {
    LOVE(0) {
        @Override
        public Score advance(Score otherPlayerScore) {
            return FIFTEEN;
        }
    },
    FIFTEEN(15) {
        @Override
        public Score advance(Score otherPlayerScore) {
            return THIRTY;
        }
    },
    THIRTY(30) {
        @Override
        public Score advance(Score otherPlayerScore) {
            return FOURTY;
        }
    },
    FOURTY(40) {
        @Override
        public Score advance(Score otherPlayerScore) {
            return END_WIN;
        }
    },
    DEUCE(45) {
        @Override
        public Score advance(Score otherPlayerScore) {
            return ADVANTAGE;
        }
    },
    ADVANTAGE(50) {
        @Override
        public Score advance(Score otherPlayerScore) {
            return END_WIN;
        }
    },
    END_WIN(Integer.MAX_VALUE) {
        @Override
        public Score advance(Score otherPlayerScore) {
            return LOVE;
        }
    };

    public final int value;

    Score(int value) {
        this.value = value;
    }

}
