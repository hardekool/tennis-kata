package com.entelect.tennis;

class Game {

    private final Player player1;
    private final Player player2;

    Game(Player player1, Player player2) {
        this.player1 = player1;
        this.player2 = player2;
        this.player1.setScore(Score.LOVE);
        this.player2.setScore(Score.LOVE);
    }

    GameScore getScore() {
        return new GameScore(player1, player2);
    }

    void winBall(Player winningPlayer) {
        if (player1.equals(winningPlayer)) {
            assignScore(player1, player2);
        } else {
            assignScore(player2, player1);
        }
    }

    private static void assignScore(Player winningPlayer, Player otherPlayer) {
        Score winningPlayerScore = winningPlayer.getScore();
        Score otherPlayerScore = otherPlayer.getScore();
        Score nextScore = winningPlayerScore.advance(otherPlayerScore);
        if (isDeuce(otherPlayerScore, nextScore)) {
            nextScore = Score.DEUCE;
            otherPlayerScore = Score.DEUCE;
        }
        winningPlayer.setScore(nextScore);
        otherPlayer.setScore(otherPlayerScore);
    }

    private static boolean isDeuce(Score otherPlayerScore, Score nextScore) {
        return nextScore.value >= Score.FOURTY.value && otherPlayerScore == nextScore;
    }

    public static class GameScore {

        private final Player player1;
        private final Player player2;

        GameScore(Player player1, Player player2) {
            this.player1 = player1;
            this.player2 = player2;
        }

        Score getP1Score() {
            return player1.getScore();
        }

        Score getP2Score() {
            return player2.getScore();
        }

        @Override
        public String toString() {
            return "GameScore{" +
                    "p1Score= " + player1.toString() +
                    ", p2Score= " + player2.toString() +
                    '}';
        }
    }

}
