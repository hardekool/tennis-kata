package com.entelect.tennis;

public interface ScoreAdvance {
    Score advance(Score otherPlayerScore);
}