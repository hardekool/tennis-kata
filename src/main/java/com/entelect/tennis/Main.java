package com.entelect.tennis;

import java.security.SecureRandom;
import java.util.Scanner;

public class Main {

    private static SecureRandom secureRandom;

    public static void main(String[] args) {
        secureRandom = new SecureRandom();
        System.out.println("Tennis!");
        if (args.length == 2) {
            playGameManual(new Player(args[0]), new Player(args[1]));
        } else if (args.length == 3){
            playGameAuto(new Player(args[0]), new Player(args[1]));
        } else {
            System.out.println("Missing arguments. Usage:\n Manual Game: \n $java -jar tennis-kata.jar john jill\n Automatic Game:\n $java -jar tennis-kata.jar john jill 1");
        }
    }

    private static void playGameManual(Player player1, Player player2) {
        Game game;
        game = new Game(player1, player2);

        Scanner scanner = new Scanner(System.in);

        System.out.println("Start game? y/n");
        String input = scanner.next();
        if (input.equalsIgnoreCase("y")) {

            Game.GameScore score;
            do {
                System.out.println("Who wins this round? 1/2");
                input = scanner.next();
                if (input.equalsIgnoreCase("1")) {
                    game.winBall(player1);
                } else {
                    game.winBall(player2);
                }
                score = game.getScore();
                System.out.println(score);
                System.out.println("===============================");
            } while (score.getP1Score() != Score.END_WIN && score.getP2Score() != Score.END_WIN);
            playGameManual(player1, player2);
        }
    }

    private static void playGameAuto(Player player1, Player player2) {
        Game game;
        game = new Game(player1, player2);

        Scanner scanner = new Scanner(System.in);

        System.out.println("Start game? y/n");
        String input = scanner.next();
        if (input.equalsIgnoreCase("y")) {

            Game.GameScore score;
            do {
                Player randomPlayer = guessRandomPlayer(player1, player2);
                System.out.println(randomPlayer.toString() + " wins this round!\n");
                game.winBall(randomPlayer);
                score = game.getScore();
                System.out.println(score);
                System.out.println("===============================");
            } while (score.getP1Score() != Score.END_WIN && score.getP2Score() != Score.END_WIN);
            playGameAuto(player1, player2);
        }
    }

    private static Player guessRandomPlayer(Player player1, Player player2) {
        int i = secureRandom.nextInt(10);
        if (i > 4) {
            return player1;
        } else {
            return player2;
        }
    }
}
